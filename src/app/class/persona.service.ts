import { Injectable } from '@angular/core';
import { IPersona } from '../models/persona.model';

@Injectable()
export class Persona implements IPersona {

  private first_name: String;
  private last_name: String;
  private age: Number;
  private gender: Boolean; // true = Maculino, False = femenino

  constructor() {
    this.first_name = 'Absner';
    this.last_name  = 'Anaya';
    this.age        = 27;
    this.gender     = true;
   }


  /*public setFirst_name(data: String) {
    this.first_name = data;
  }

  public setLast_name(data: String) {
    this.last_name  = data;
  }

  public setAge(data: Number) {
    this.age  = data;
  }

  public setGender(data: Boolean) {
    this.gender = data;
  }*/

  /**
   * getFirst_name
   */
  public getFirst_name() {
    return this.first_name;
  }
  /**
   * getLast_name
   */
  public getLast_name() {
    return this.last_name;
  }
  /**
   * getAge
   */
  public getAge() {
    return this.age;
  }
  /**
   * getGender
   */
  public getGender() {
    return this.gender;
  }

}
